import { Injectable } from '@angular/core';
import { Pokemon } from '../models/pokemon.models';


@Injectable({
  providedIn: 'root'
})
export class PokeStorageService {

  private thePokemon: Pokemon = null;

  addPokemon(pokemon: Pokemon): void {
    this.thePokemon = pokemon;
  }

  getPokemon(): Pokemon {
    return this.thePokemon;
  }

  constructor() { }
}
