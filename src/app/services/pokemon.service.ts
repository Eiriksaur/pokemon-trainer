import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  constructor(private http: HttpClient) { }

  getAllPokemon(offset): Promise<any>{
    return this.http.get(`https://pokeapi.co/api/v2/pokemon/?limit=10&offset=${offset}`).toPromise();
  }
  getOnePokemon(name: string): Promise<any>{
    return this.http.get(`https://pokeapi.co/api/v2/pokemon/${name}`).toPromise();
  }
  // Running multiple promises at once
  public getMultiplePokemon( names: string[] ) {

    const pokePromises = names.map(name => this.getOnePokemon(name));

    // 10 promises.
    return Promise.all([...pokePromises]);
  }
}
