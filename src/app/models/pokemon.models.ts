export interface Pokemon {
    name: string;
    height: number;
    weight: number;
    sprite: string;
    types: string[];
    moves: string[];
    baseStats: string[];
    abilities: string[];
    baseExp: number;
}