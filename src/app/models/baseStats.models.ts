export interface BaseStats {
    HP: string;
    Speed: string;
    Attack: string;
    Defence: string;
    SpecDef: string;
    SpecAttack: string;
}