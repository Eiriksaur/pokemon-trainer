import { Component, OnInit } from '@angular/core';
import {setStorage} from '../../../../utils/storage'

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  username: String = "";

  constructor() { }

  ngOnInit(): void {

  }

  usernameChanged(event): void{
    this.username = event.target.value;
  }
  sendUsername():void{
    setStorage(this.username)

  }

}
