import { Component, OnInit, Input } from '@angular/core';
import { PokeStorageService } from 'src/app/services/poke-storage.service';
import { Pokemon } from '../../../models/pokemon.models'

@Component({
  selector: 'app-pokemon-card',
  templateUrl: './pokemon-card.component.html',
  styleUrls: ['./pokemon-card.component.css']
})

export class PokemonCardComponent implements OnInit {

@Input() pokemon: Pokemon;

  constructor(private pokestorageService: PokeStorageService) { }

  ngOnInit(): void {
  }

  storePokemon(pokemon: Pokemon): void{
    this.pokestorageService.addPokemon(pokemon);
  }


}
