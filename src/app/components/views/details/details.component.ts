import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Pokemon } from 'src/app/models/pokemon.models';
import { PokeStorageService } from 'src/app/services/poke-storage.service';
import { storePokemon, getPokemons } from 'src/utils/storage';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  thePokemon: Pokemon;

  constructor(private route: ActivatedRoute, private pokeStorageService: PokeStorageService) { }

  ngOnInit(): void {
    this.thePokemon = this.pokeStorageService.getPokemon();
  }

  catch(pokemon: Pokemon): void{
    console.log(pokemon)
    storePokemon(pokemon);
  }

}
