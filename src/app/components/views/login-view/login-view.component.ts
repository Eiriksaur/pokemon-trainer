import { Component, OnInit } from '@angular/core';
import { getStorage } from 'src/utils/storage'
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-view',
  templateUrl: './login-view.component.html',
  styleUrls: ['./login-view.component.css']
})
export class LoginViewComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.checkIfLoggedIn();
  }

  checkIfLoggedIn(){
    if(getStorage('trainername')){
      this.router.navigateByUrl('/catalogue')
    }
  }

}
