import { Component, OnInit } from '@angular/core';
import { PokemonService } from '../../../services/pokemon.service';
import { Pokemon } from 'src/app/models/pokemon.models';
import { SelectMultipleControlValueAccessor } from '@angular/forms';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue-view.component.html',
  styleUrls: ['./catalogue-view.component.css']
})
export class CatalogueViewComponent implements OnInit {

  offset: number = 0;

  pokeList: Pokemon[] = [];

  pokeNames: string[] = []

  statList: any = [];

  thePokemon: Pokemon = {
    name: '',
    height: 0,
    weight: 0,
    types: [],
    moves: [],
    abilities: [],
    baseStats: [],
    sprite: '',
    baseExp: 0
  };

  constructor(private pokemonService: PokemonService) { }

  async ngOnInit() {
    await this.getPokemonNames(this.offset)
    await this.getPokemons(this.pokeNames)
  }

  async getPokemonNames(offset): Promise<any> {
    try {
      this.pokeList = [];
      this.pokeNames = await this.pokemonService.getAllPokemon(offset).then(r=>r.results).then(pokemons => {
        return pokemons.map((p: Pokemon)  => p.name)
      });

    } catch (error) {
      console.log(error)
    }
  }

  getPokemons = async (pokemonNames) => {

    try {
      const results = await this.pokemonService.getMultiplePokemon( pokemonNames );
      for(let i = 0; i < results.length; i++){
        this.pokeList.push(Object.assign({},this.createPokemon(results[i])))
      }
    } catch (e) {
      console.error(e);
    }
  }

  createPokemon(pokemonReq: any): Pokemon{
    let tempPokemon: Pokemon = this.thePokemon;
    tempPokemon.height = pokemonReq.height;
    tempPokemon.weight = pokemonReq.weight;
    tempPokemon.name = pokemonReq.name;
    tempPokemon.sprite = pokemonReq.sprites.front_default;
    tempPokemon.types = pokemonReq.types;
    tempPokemon.baseStats = pokemonReq.stats[0].base_stat;
    this.statList= pokemonReq.moves;
    let tempList = [];
    for(let i = 0; i < this.statList.length; i++){
      tempList.push(`${this.statList[i].move.name}`)
    }
    tempPokemon.moves = tempList;
    this.statList = pokemonReq.abilities;
    tempList = [];
    for(let i = 0; i < this.statList.length; i++){
      tempList.push(`${this.statList[i].ability.name}`)
    }
    tempPokemon.abilities = tempList;
    tempPokemon.baseExp = pokemonReq.base_experience;
    this.statList = pokemonReq.stats;
    tempList = [];
    for(let i = 0; i < this.statList.length; i++){
      tempList.push(`${this.statList[i].stat.name}: ${this.statList[i].base_stat}`)
    }
    tempPokemon.baseStats = tempList;
    return tempPokemon;
  }

  async getNextSetOfPokemon(): Promise<any>{
    this.offset += 10;
    await this.getPokemonNames(this.offset);
    await this.getPokemons(this.pokeNames)
  }
  async getPreviousSetOfPokemon(): Promise<any>{
    if(this.offset > 9) this.offset -= 10;
    await this.getPokemonNames(this.offset);
    await this.getPokemons(this.pokeNames)
  }
}
