import { Component, OnInit } from '@angular/core';
import {getPokemons, getStorage } from 'src/utils/storage';
import { Pokemon } from 'src/app/models/pokemon.models';

@Component({
  selector: 'app-trainer-view',
  templateUrl: './trainer-view.component.html',
  styleUrls: ['./trainer-view.component.css']
})
export class TrainerViewComponent implements OnInit {

  pokeArray: Pokemon[] = [];

  name: string = "";

  constructor() { }

  ngOnInit(): void {
    this.getPokemonsFromStorage();
    this.getTrainername();
  }

  getTrainername(){
    this.name = getStorage('trainername')
  }

  getPokemonsFromStorage(){
    this.pokeArray = getPokemons();
  }

}
