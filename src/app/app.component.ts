import { Component } from '@angular/core';
import { emptyStorage } from 'src/utils/storage.js'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'pokemon-trainer';

  logout(){
    emptyStorage();
  }
}
