import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginViewComponent } from './components/views/login-view/login-view.component'
import { CatalogueViewComponent } from './components/views/catalogue-view/catalogue-view.component'
import { DetailsComponent } from './components/views/details/details.component'
import { TrainerViewComponent } from './components/views/trainer-view/trainer-view.component';
import { AuthGuard } from 'src/app/guards/auth/auth.guard'

const routes: Routes = [
  { 
    path: 'login', 
    component: LoginViewComponent 
  },
  { 
    path: 'catalogue', 
    component: CatalogueViewComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'details',
    component: DetailsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'profile',
    component: TrainerViewComponent,
    canActivate: [AuthGuard]
  },
  { 
    path: '', 
    pathMatch: 'full', 
    redirectTo: '/login'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
