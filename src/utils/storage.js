export const setStorage = (value) => {
    localStorage.setItem('trainername', JSON.stringify(value));
}

export const getStorage = (key) => {
    const storedValue = localStorage.getItem(key);
    if(storedValue) return JSON.parse(storedValue);
    return false;
}

export const getPokemons = () => {
    const storedValue = localStorage.getItem('pokemons');
    if(storedValue){
        return JSON.parse(storedValue);
    }
    return [];
}

export const emptyStorage = () =>{
    localStorage.clear();
}

export const storePokemon =(pokemon) =>{
    let pokeList = getPokemons();
    if(!pokeList){
        localStorage.setItem('pokemons', JSON.stringify(pokeList))
    } else {
        let tempList = [];
        tempList = [...pokeList];
        tempList.push(pokemon);
        localStorage.setItem('pokemons', JSON.stringify(tempList))
    }
}